#!/bin/sh
# ASCII code to character
printChar() {
  printf \\`printf '%03o' "$1"`
}

# Iterate through all printable characters
i=32
while [ "$i" -lt 127 ]; do
  res=`printChar $i | tr $*`
  # If $i fits the tr rule
  if [ ! -z "$res" ]; then
    echo $res
  fi
  i=$(($i + 1))
done

#/bin/sh
out=""
# For each character in the template ($1)
for type in `echo "$1" | sed -e 's/\(.\)/\1\n/g'`; do
  # Generate a word from the $type-th list and append it to the output
  cmd="./diceGen.sh 1 \$$type"
  out="$out`eval $cmd`"
done
echo $out

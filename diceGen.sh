#!/bin/sh
. ./randLib.sh
# Number of lines in the wordlist
lines=`wc -l $2 | cut -f1 -d' '`
j=0
out=""
while [ "$j" -lt "$1" ]; do
  range=+$((`rand $lines`))
  # Append n-th line to output
  out="$out`tail -n$range $2 | head -n1`"
  j=$(($j + 1))
done
echo "$out"

#!/bin/sh
# Returns a random integer [0, 65536)
randRaw() {
  od -A n -t u -N 2 /dev/random
}
# Returns a random integer [1, $lines]
rand() {
  # Filter out numbers that are too big to avoid bias when doing modulus
  thres=$((65536 - (65536 % $1)))
  while res=`randRaw`; [ "$res" -ge "$thres" ]; do :; done
	res=$(($res % $1 + 1))
  echo $res
}

#/bin/sh
. ./randLib.sh
# Get string from stdin
str=`cat`
# There are $len + 1 spaces to choose from
len=$((${#str} + 1))
# Subtract one to make it start from 0
pos=$((`rand $len` - 1))
# Produce the random character and escape problematic ones (/\&)
chr=`./diceGen.sh 1 $2 | sed -e 's/[\/&]/\\\&/g'`
out=`echo $str | sed -e 's/\(.\{'$pos'\}\)\(.*\)/\1'$chr'\2/g'`
# Recursively add more characters
if [ "$1" -le 1 ]; then
  echo $out
else
  echo $out | ./randChar.sh $(($1 - 1)) $2
fi
